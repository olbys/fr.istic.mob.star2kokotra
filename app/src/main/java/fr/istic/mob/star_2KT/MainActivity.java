package fr.istic.mob.star_2KT;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import fr.istic.mob.star_2KT.splash_.ControleFragment;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class MainActivity extends AppCompatActivity  implements RadialTimePickerDialogFragment.OnTimeSetListener,
        CalendarDatePickerDialogFragment.OnDateSetListener  {

    Calendar now ;
    SimpleDateFormat dateFormat;

    Button btn_horaire_select;
    Button btn_date_select;
    String text_full_date =" ";


    private static final String FRAG_TAG_DATE_PICKER = "fragment_date_picker_name";
    private static final String FRAG_TIME_PICKER = "timePickerDialogFragment";
    private static final String PATTERN_DATE = "EEEE, d MMMM yyyy HH:mm";

    FragmentManager fragmentManager ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         Toolbar mTopToolbar =  findViewById(R.id.my_toolbar);
         setSupportActionBar(mTopToolbar);

         getSupportActionBar().setDisplayShowTitleEnabled(false);
         getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btn_horaire_select = findViewById(R.id.btn_horaire_select);
        now = Calendar.getInstance();
        dateFormat = new SimpleDateFormat(PATTERN_DATE);


        btn_horaire_select.setOnClickListener(onClickListenerHoraire);


        btn_date_select = findViewById(R.id.btn_date_select);
        btn_date_select.setOnClickListener(onClickListenerDate);

        // Gestion des fragments
        fragmentManager  =  getSupportFragmentManager();
        ControleFragment controleFragment =  (ControleFragment)
                fragmentManager.findFragmentById(R.id.frament_one_space);


    }



    // button de selection des dates
    private View.OnClickListener onClickListenerDate = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                    .setOnDateSetListener(MainActivity.this)
                    .setThemeDark();
            cdp.show(getSupportFragmentManager(), FRAG_TAG_DATE_PICKER);
        }
    };



    // button de selection des dates
    private View.OnClickListener onClickListenerHoraire = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            now = Calendar.getInstance();
            int hour = now.get(Calendar.HOUR_OF_DAY);
            int minute = now.get(Calendar.MINUTE);
            RadialTimePickerDialogFragment radial = new RadialTimePickerDialogFragment()
                    .setOnTimeSetListener(MainActivity.this)
                    .setStartTime(hour, minute)
                    .setDoneText(getString(R.string.text_ok))
                    .setCancelText(getString(R.string.text_cancel))
                    .setForced24hFormat()
                    .setThemeDark();
            radial.show(getSupportFragmentManager(), FRAG_TIME_PICKER);
        }
    };


    @Override
    public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
        now = Calendar.getInstance();
        now.set(Calendar.HOUR, hourOfDay);
        now.set(Calendar.MINUTE, minute);
        text_full_date = (dateFormat.format(now.getTime()));
//        text_view_full_date.setText(dateFormat.format(now.getTime()));

    }


    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        now = Calendar.getInstance();
        now.set(Calendar.YEAR, year);

        now.set(Calendar.MONTH, monthOfYear);
        now.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        text_full_date = dateFormat.format(now.getTime());
        // text_view_full_date.setText(dateFormat.format(now.getTime()));

    }
}
