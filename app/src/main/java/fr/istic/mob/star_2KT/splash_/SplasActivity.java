package fr.istic.mob.star_2KT.splash_;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import fr.istic.mob.star_2KT.MainActivity;
import fr.istic.mob.star_2KT.R;


public class SplasActivity extends Activity {

    private static int SPLASH_TIMEOUT = 4000;
    private Handler myHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        Animation anim = AnimationUtils.loadAnimation(this , R.anim.splash_anim);
        ImageView logo = (ImageView) findViewById(R.id.splash_image);
        logo.setAnimation(anim);

        myHandler = new Handler();

        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplasActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        } , SPLASH_TIMEOUT);

    }
}
